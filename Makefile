export HOST=$(shell hostname)
export CURRDIR=$(shell pwd)

seadas: seadas.v7.5.recipe
		rm -rf seadas.v7.5
		sudo singularity build seadas.v7.5 seadas.v7.5.recipe

test:
	##############################################################################
	# Testing if we can update LUTS
	##############################################################################
	singularity exec -B $(CURRDIR):/opt/seadas/var/ seadas.v7.5 update_luts.py -v viirsn
	# singularity exec -B $(CURRDIR):/opt/seadas/var/ seadas.v7.5 /opt/seadas/update_viirs_luts.sh
	if [ -d "./viirsn" ]; then echo "++| Created viirs dir correctly"; else echo "--| Did not create viirsn dir"; exit 1 ;fi
	if [ -f "./common/polar_wander.ascii" ]; then echo "++| Created polar wander file correctly"; else echo "--| Did not create polar wander file"; exit 1; fi
