# SeaDAS Container Recipe

This is the NASA SeaDAS Ocean Color processing package in a Singularity container.

## Singularity:
https://www.sylabs.io/singularity/

## SeaDAS:

https://seadas.gsfc.nasa.gov/

SeaDAS is a comprehensive software package for the processing, analysis, and quality control of remote-sensing Earth data.

Once built, This container holds the science processing component of SeaDAS (OceanColor Science Software or OCSSW: https://oceandata.sci.gsfc.nasa.gov/ocssw/) , which applies the OBPG algorithms to satellite data in order to characterize and calibrate the data and generate science quality OBPG products. SeaDAS currently supports the science processing of over 15 US and international satellite missions.

At the time of writing, the latest version of SeaDAS is 7.5.3 and this container has SeaDAS scientific processing code installed in:
  `/opt/seadas/`

N.B. This image only has the `modis-aqua` and `npp viirs` packages installed at the moment. You will need to modifiy the image recipe if you want to install other sensors - this is only doen because building an image with all sensor packages makes a huge image and i couldn't be bothered waiting for it to build.

## Building:
TODO

## Examples:
TODO
